# Kaniko Template

This is a template that builds docker images inside the CI pipeline using Kaniko and pushes them to the GitLab container registry.

More information about Kaniko: [https://github.com/GoogleContainerTools/kaniko](https://github.com/GoogleContainerTools/kaniko).
