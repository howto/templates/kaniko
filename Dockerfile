FROM alpine:latest

ENTRYPOINT [ "/bin/echo" ]
CMD [ "Hello, I was created in the CI pipeline!" ]
